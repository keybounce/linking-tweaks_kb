package org.bitbucket.linkingtweaks;

import com.xcompwiz.mystcraft.api.event.PortalLinkEvent;
import com.xcompwiz.mystcraft.api.event.StarFissureLinkEvent;
import net.minecraftforge.event.ForgeSubscribe;
import org.bitbucket.linkingtweaks.api.lib.LinkFlags;

public class ListenerAddFlags {
    @ForgeSubscribe
    public void addPortalFlag(PortalLinkEvent event) {
        Log.logFiner("Possible portal link initiated by %s!", event.entity.getEntityName());
        event.info.setFlag(LinkFlags.PORTAL.flag, true);
    }

    @ForgeSubscribe
    public void addFissureFlag(StarFissureLinkEvent event) {
        Log.logFiner("Possible star fissure link initiated by %s!", event.entity.getEntityName());
        event.info.setFlag(LinkFlags.FISSURE.flag, true);
    }
}
