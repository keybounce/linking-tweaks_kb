package org.bitbucket.linkingtweaks;

import cpw.mods.fml.common.FMLLog;

import java.util.logging.Level;

public class Log {
    public static void logSevere(String format, Object... data) {
        log(Level.SEVERE, format, data);
    }

    public static void logWarning(String format, Object... data) {
        log(Level.WARNING, format, data);
    }

    public static void logInfo(String format, Object... data) {
        log(Level.INFO, format, data);
    }

    public static void logFine(String format, Object... data) {
        log(Level.FINE, format, data);
    }

    public static void logFiner(String format, Object... data) {
        log(Level.FINER, format, data);
    }

    public static void logFinest(String format, Object... data) {
        log(Level.FINEST, format, data);
    }

    public static void log(Level level, String format, Object... data) {
        FMLLog.log(level, format, data);
    }
}
