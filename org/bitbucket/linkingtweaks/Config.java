package org.bitbucket.linkingtweaks;

import cpw.mods.fml.common.Loader;
import net.minecraftforge.common.Configuration;
import net.minecraftforge.common.Property;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class Config {
    public final String CATEGORY_TWEAKS = "general";

    private final File CFG_ROOT = new File(Loader.instance().getConfigDir(), "mystcraft/");
    private final Configuration CFG;

    private Map<String, Boolean> enabledTweaks = new HashMap<String, Boolean>();
    private Map<String, Integer> integerConfigs = new HashMap<String, Integer>();
    private Map<String, int[]> integerArrayConfigs = new HashMap<String, int[]>();
    private Map<String, Boolean> booleanConfigs = new HashMap<String, Boolean>();

    public Config(String fileName) {
        CFG = new Configuration(new File(CFG_ROOT, fileName));
    }

    public boolean addTweakConfig(String name, String comment, boolean value) {
        Property property = CFG.get(CATEGORY_TWEAKS, name, value);
        property.comment = comment;
        value = property.getBoolean(value);
        enabledTweaks.put(name, value);
        CFG.save();
        return value;
    }

    public int addIntConfig(String name, String comment, int value) {
        Property property = CFG.get(CATEGORY_TWEAKS, name, value);
        property.comment = comment;
        value = property.getInt(value);
        integerConfigs.put(name, value);
        CFG.save();
        return value;
    }

    public int[] addIntArrayConfig(String name, String comment, int... values) {
        Property property = CFG.get(CATEGORY_TWEAKS, name, values);
        property.comment = comment;
        if(property.isIntList()) {
            values = property.getIntList();
            integerArrayConfigs.put(name, values);
            CFG.save();
        }
        return values;

    }

    public boolean addBooleanConfig(String name, String comment, boolean value) {
        Property property = CFG.get(CATEGORY_TWEAKS, name, value);
        property.comment = comment;
        value = property.getBoolean(value);
        booleanConfigs.put(name, value);
        CFG.save();
        return value;
    }

    public void save() {
        CFG.save();
    }

    public boolean isTweakEnabled(String name) {
        boolean value = false;
        if(enabledTweaks.containsKey(name)) {
            value = enabledTweaks.get(name);
        }
        return value;
    }

    public int getIntConfig(String name) {
        int value = 0;
        if(integerConfigs.containsKey(name)) {
            value = integerConfigs.get(name);
        }
        return value;
    }

    public int[] getIntArrayConfig(String name) {
        return integerArrayConfigs.get(name);
    }

    public boolean getBooleanConfig(String name) {
        boolean value = false;
        if(booleanConfigs.containsKey(name)) {
            value = booleanConfigs.get(name);
        }
        return value;
    }
}
