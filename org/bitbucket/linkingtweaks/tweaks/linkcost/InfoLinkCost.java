package org.bitbucket.linkingtweaks.tweaks.linkcost;

import org.bitbucket.linkingtweaks.LinkingTweaks;
import org.bitbucket.linkingtweaks.tweaks.TweakInfo;

public class InfoLinkCost extends TweakInfo {
    public final int basic;
    public final int intra;
    public final int follow;
    public final int portal;
    public final int disarm;

    public InfoLinkCost() {
        CONFIG_NAME = "linkcost";
        CONFIG_COMMENT = "Linking info experience levels.  Cost is the sum of the cost of all the link's traits, or 0, whichever is highest.";
        enabled = LinkingTweaks.CFG.addTweakConfig(CONFIG_NAME, CONFIG_COMMENT, false);

        basic = getConfigValueOrDefault("base", 1);
        intra = getConfigValueOrDefault("intra", 1);
        follow = getConfigValueOrDefault("following", 2);
        portal = getConfigValueOrDefault("portal", -1);
        disarm = getConfigValueOrDefault("disarm", -2);
    }

    public int getConfigValueOrDefault(String name, int defaultCost) {
        return LinkingTweaks.CFG.addIntConfig(CONFIG_NAME + ".trait." + name, null, defaultCost);
    }
}