package org.bitbucket.linkingtweaks.tweaks.linkcost;

import org.bitbucket.linkingtweaks.tweaks.Tweak;
import org.bitbucket.linkingtweaks.tweaks.TweakInfo;

public class TweakLinkCost extends Tweak {
    static InfoLinkCost info;

    public TweakLinkCost() {
        super(new ListenerLinkCost());
    }

    @Override
    public TweakInfo getInfo() {
        return info;
    }

    @Override
    public void configure() {
        info = new InfoLinkCost();
    }
}
