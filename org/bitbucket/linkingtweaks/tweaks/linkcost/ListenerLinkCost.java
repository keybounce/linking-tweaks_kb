package org.bitbucket.linkingtweaks.tweaks.linkcost;

import com.xcompwiz.mystcraft.api.linking.LinkEvent;
import com.xcompwiz.mystcraft.api.linking.LinkEvent.LinkEventAllow;
import com.xcompwiz.mystcraft.api.linking.LinkEvent.LinkEventStart;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.event.ForgeSubscribe;
import org.bitbucket.linkingtweaks.api.MystLib;
import org.bitbucket.linkingtweaks.api.lib.LinkFlags;
import org.bitbucket.linkingtweaks.tweaks.TweakListener;

public class ListenerLinkCost implements TweakListener {
    private String[] bypassFlags = {LinkFlags.NATURAL.flag, LinkFlags.TPX.flag};

    @ForgeSubscribe
    public void validateLink(LinkEventAllow event) {
        if(TweakLinkCost.info.isEnabled()) {
            if(event.entity instanceof EntityPlayer) {
                EntityPlayer player = (EntityPlayer) event.entity;
                if(player.experienceLevel < determineLinkCost(event) && !containsBypass(event)) {
                    event.setCanceled(true);
                }
            }
        }
    }

    @ForgeSubscribe
    public void chargeLevels(LinkEventStart event) {
        if(TweakLinkCost.info.isEnabled()) {
            if(event.entity instanceof EntityPlayer) {
                EntityPlayer player = (EntityPlayer) event.entity;
                if(LinkFlags.doesNotContainFlags(event.options, bypassFlags)) {
                    player.experienceLevel -= determineLinkCost(event);
                }
            }
        }
    }

    private int determineLinkCost(LinkEvent event) {
        int cost = TweakLinkCost.info.basic;
        cost += addIntraCost(event);
        cost += addFollowCost(event);
        cost += addPortalCost(event);
        cost += addDisarmCost(event);
        return Math.max(cost, 0);
    }

    private int addIntraCost(LinkEvent event) {
        int addend = 0;
        if(MystLib.ages.isIntraLink(MystLib.ages.getDimIdFromEntity(event.entity), event.options.getDimensionUID())) {
            addend = TweakLinkCost.info.intra;
        }
        return addend;
    }

    private int addFollowCost(LinkEvent event) {
        int addend = 0;
        //TODO: Properly check if the linking object actually follows, rather than checking that it has the ability to do so.
        if(LinkFlags.linkContainsFlags(event.options, LinkFlags.FOLLOW.flag)) {
            addend = TweakLinkCost.info.follow;
        }
        return addend;
    }

    private int addPortalCost(LinkEvent event) {
        int addend = 0;
        if(LinkFlags.linkContainsFlags(event.options, LinkFlags.PORTAL.flag)) {
            addend = TweakLinkCost.info.portal;
        }
        return addend;
    }

    private int addDisarmCost(LinkEvent event) {
        int addend = 0;
        if(LinkFlags.linkContainsFlags(event.options, LinkFlags.DISARM.flag)) {
            addend = TweakLinkCost.info.disarm;
        }
        return addend;
    }

    private boolean containsBypass(LinkEvent event) {
        boolean value = false;
        if(LinkFlags.linkContainsFlags(event.options, bypassFlags)) {
            value = true;
        } else if(LinkFlags.linkContainsFlags(event.options, LinkFlags.EXTERNAL.flag) && LinkFlags.doesNotContainFlags(event.options, LinkFlags.PORTAL.flag)) {
            value = true;
        }
        return value;
    }
}
