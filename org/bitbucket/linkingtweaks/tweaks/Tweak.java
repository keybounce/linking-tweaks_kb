package org.bitbucket.linkingtweaks.tweaks;

import net.minecraftforge.common.MinecraftForge;
import org.bitbucket.linkingtweaks.Log;

/**
 * The base class for all general, allowing for unified tweak handling.
 *
 * @author Veovis Muad'dib
 */
public abstract class Tweak {
    protected TweakListener listener;

    public Tweak(TweakListener listener) {
        this.listener = listener;
    }

    public final String getConfigName() {
        return getInfo().getName();
    }

    public final String getConfigComment() {
        return getInfo().getComment();
    }

    public abstract TweakInfo getInfo();

    public abstract void configure();

    public void registerListeners() {
        Log.logInfo("Registering %s!", getInfo().CONFIG_NAME);
        MinecraftForge.EVENT_BUS.register(listener);
    }

    public void registerAPI() {
    }
}