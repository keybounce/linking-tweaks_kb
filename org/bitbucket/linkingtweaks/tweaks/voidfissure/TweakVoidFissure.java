package org.bitbucket.linkingtweaks.tweaks.voidfissure;

import org.bitbucket.linkingtweaks.tweaks.Tweak;
import org.bitbucket.linkingtweaks.tweaks.TweakInfo;

/**
 * Causes living entities falling into void to link as though via Star Fissure to Overworld spawn.
 * Does not apply to items.
 *
 * @author Veovis Muad'dib
 */
public class TweakVoidFissure extends Tweak {
    static InfoVoidFissure info;

    public TweakVoidFissure() {
        super(new ListenerVoidFissure());
    }

    @Override
    public TweakInfo getInfo() {
        return info;
    }

    @Override
    public void configure() {
        info = new InfoVoidFissure();
    }
}
