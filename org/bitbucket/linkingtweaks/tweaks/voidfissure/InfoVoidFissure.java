package org.bitbucket.linkingtweaks.tweaks.voidfissure;

import org.bitbucket.linkingtweaks.LinkingTweaks;
import org.bitbucket.linkingtweaks.tweaks.TweakInfo;

/** @author Veovis Muad'dib */
public class InfoVoidFissure extends TweakInfo {
    InfoVoidFissure() {
        CONFIG_NAME = "voidfissure";
        CONFIG_COMMENT = "Living entities falling through the void at the bottom of the world will be linked as though " + "through a Star Fissure.";
        enabled = LinkingTweaks.CFG.addTweakConfig(CONFIG_NAME, CONFIG_COMMENT, false);
    }
}
