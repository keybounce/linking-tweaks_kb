package org.bitbucket.linkingtweaks.tweaks.voidfissure;

import com.xcompwiz.mystcraft.api.MystAPI;
import com.xcompwiz.mystcraft.api.linking.ILinkInfo;
import net.minecraft.util.DamageSource;
import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import org.bitbucket.linkingtweaks.Log;
import org.bitbucket.linkingtweaks.api.MystLib;
import org.bitbucket.linkingtweaks.tweaks.TweakListener;

public class ListenerVoidFissure implements TweakListener {
    @ForgeSubscribe
    public void voidAsFissure(LivingHurtEvent event) {
        if(TweakVoidFissure.info.isEnabled()) {
            if(event.source == DamageSource.outOfWorld) {
                event.setCanceled(true);
                logRedirectedEntity(event);
                ILinkInfo link = MystLib.linking.createFissureLink();
                MystAPI.linking.linkEntity(event.entityLiving, link);
            }
        }
    }

    private void logRedirectedEntity(LivingHurtEvent event) {
        Log.logFine("Entity %s fell through to void layer!  Redirecting to Overworld spawn.", event.entity.getEntityName());
    }
}
