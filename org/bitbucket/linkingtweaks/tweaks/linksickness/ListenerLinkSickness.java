package org.bitbucket.linkingtweaks.tweaks.linksickness;

import com.xcompwiz.mystcraft.api.linking.LinkEvent;
import com.xcompwiz.mystcraft.api.linking.LinkEvent.LinkEventEnterWorld;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.potion.PotionEffect;
import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import org.bitbucket.linkingtweaks.api.lib.LinkFlags;
import org.bitbucket.linkingtweaks.tweaks.TweakListener;

import java.util.Map;
import java.util.WeakHashMap;

public class ListenerLinkSickness implements TweakListener {
    private String[] bypassFlags = {LinkFlags.NATURAL.flag, LinkFlags.TPX.flag};
    private Map<EntityLivingBase, Integer> sickEntities = new WeakHashMap<EntityLivingBase, Integer>();

    @ForgeSubscribe
    public void listenForLinkingEntities(LinkEventEnterWorld event) {
        if(shouldProcessLink(event)) {
            int points = determineSicknessPointsToAdd(event);
            addSicknessPointsToEntity((EntityLivingBase) event.entity, points);
        }
    }

    @ForgeSubscribe
    public void updateSickness(LivingUpdateEvent event) {
        if(shouldProcessUpdate(event)) {
            applySicknesses(event.entityLiving);
            addSicknessPointsToEntity(event.entityLiving, -1);
        }

    }

    private boolean shouldProcessLink(LinkEvent event) {
        boolean value = false;
        if(TweakLinkSickness.info.isEnabled()) {
            if(LinkFlags.doesNotContainFlags(event.options, bypassFlags)) {
                if(event.entity instanceof EntityLivingBase) {
                    value = true;
                }
            }
        }
        return value;
    }

    private boolean shouldProcessUpdate(LivingUpdateEvent event) {
        boolean value = false;
        if(event.entity.worldObj.getTotalWorldTime() % TweakLinkSickness.info.COOLDOWN_RATE == 0) {
            if(TweakLinkSickness.info.isEnabled()) {
                if(sickEntities.containsKey(event.entityLiving)) {
                    value = true;
                }
            }
        }
        return value;
    }

    private int determineSicknessPointsToAdd(LinkEvent event) {
        int points;
        if(LinkFlags.linkContainsFlags(event.options, LinkFlags.MAINTAIN_MOMENTUM.flag)) {
            points = TweakLinkSickness.info.MM_LINK_COST;
            if(LinkFlags.linkContainsFlags(event.options, LinkFlags.PORTAL.flag)) {
                points /= 2;
            }
        } else {
            points = TweakLinkSickness.info.LINK_COST;
        }
        return points;
    }

    private void addSicknessPointsToEntity(EntityLivingBase entityLiving, int points) {
        if(sickEntities.containsKey(entityLiving)) {
            points += sickEntities.get(entityLiving);
        }
        if(points > 0) {
            sickEntities.put(entityLiving, points);
        } else {
            sickEntities.remove(entityLiving);
        }
    }

    private void applySicknesses(EntityLivingBase entityLiving) {
        int points = sickEntities.get(entityLiving);
        for(PotionEffect effect : TweakLinkSickness.info.potions.keySet()) {
            int threshold = TweakLinkSickness.info.potions.get(effect);
            if(points > threshold && threshold > 0) {
                entityLiving.addPotionEffect(new PotionEffect(effect));
            }
        }
    }
}