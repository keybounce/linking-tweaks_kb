package org.bitbucket.linkingtweaks.tweaks.linksickness;

import org.bitbucket.linkingtweaks.tweaks.Tweak;
import org.bitbucket.linkingtweaks.tweaks.TweakInfo;

public class TweakLinkSickness extends Tweak {
    static InfoLinkSickness info;

    public TweakLinkSickness() {
        super(new ListenerLinkSickness());
    }

    @Override
    public TweakInfo getInfo() {
        return info;
    }

    @Override
    public void configure() {
        info = new InfoLinkSickness();
    }
}
