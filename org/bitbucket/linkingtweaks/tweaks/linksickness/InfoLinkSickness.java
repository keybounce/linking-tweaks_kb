package org.bitbucket.linkingtweaks.tweaks.linksickness;

import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import org.bitbucket.linkingtweaks.LinkingTweaks;
import org.bitbucket.linkingtweaks.tweaks.TweakInfo;

import java.util.HashMap;
import java.util.Map;


public class InfoLinkSickness extends TweakInfo {
    final int COOLDOWN_RATE;
    final int LINK_COST;
    final int MM_LINK_COST;
    final Map<PotionEffect, Integer> potions = new HashMap<PotionEffect, Integer>();

    private final int EFFECT_DURATION = 100;

    public InfoLinkSickness() {
        CONFIG_NAME = "linksickness";
        CONFIG_COMMENT = "Linking quickly causes players to accumulate sickness points. Players recover 1 point per x ticks.";
        enabled = LinkingTweaks.CFG.addTweakConfig(CONFIG_NAME, CONFIG_COMMENT, false);

        COOLDOWN_RATE = Math.max(1, getConfigValueOrDefault("pointrecoveryrate", 20));
        LINK_COST = getConfigValueOrDefault("pointsperlink.basic", 100);
        MM_LINK_COST = getConfigValueOrDefault("pointsperlink.momentum", 75);
        loadSicknessConfig(new PotionEffect(Potion.hunger.id, EFFECT_DURATION, 0, true), 140);
        loadSicknessConfig(new PotionEffect(Potion.confusion.id, EFFECT_DURATION, 0, true), 210);
        loadSicknessConfig(new PotionEffect(Potion.poison.id, EFFECT_DURATION, 0, true), 280);
        loadSicknessConfig(new PotionEffect(Potion.blindness.id, EFFECT_DURATION, 0, true), 320);
    }

    private int getConfigValueOrDefault(String name, int defaultCost) {
        return LinkingTweaks.CFG.addIntConfig("linksickness." + name, null, defaultCost);
    }

    private void loadSicknessConfig(PotionEffect effect, int threshold) {
        threshold = getConfigValueOrDefault("threshold." + effect.getEffectName().toLowerCase(), threshold);
        potions.put(effect, threshold);
    }
}
