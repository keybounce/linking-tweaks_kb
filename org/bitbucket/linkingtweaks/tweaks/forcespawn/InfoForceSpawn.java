package org.bitbucket.linkingtweaks.tweaks.forcespawn;

import org.bitbucket.linkingtweaks.LinkingTweaks;
import org.bitbucket.linkingtweaks.tweaks.TweakInfo;

/** @author Veovis Muad'dib */
public class InfoForceSpawn extends TweakInfo {
    InfoForceSpawn() {
        CONFIG_NAME = "spawnredirect";
        CONFIG_COMMENT = "Causes links to lead to the spawn point of the dimension regardless of their destination.";
        enabled = LinkingTweaks.CFG.addTweakConfig(CONFIG_NAME, CONFIG_COMMENT, false);
    }
}
