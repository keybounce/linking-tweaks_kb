package org.bitbucket.linkingtweaks.tweaks.forcespawn;

import org.bitbucket.linkingtweaks.tweaks.Tweak;
import org.bitbucket.linkingtweaks.tweaks.TweakInfo;

/**
 * Redirects links to the spawnpoint of the destination dimension.
 *
 * @author Veovis Muad'dib
 */
public class TweakForceSpawn extends Tweak {
    static InfoForceSpawn info;

    public TweakForceSpawn() {
        super(new ListenerForceSpawn());
    }

    @Override
    public TweakInfo getInfo() {
        return info;
    }

    @Override
    public void configure() {
        info = new InfoForceSpawn();
    }
}
