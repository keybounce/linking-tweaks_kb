package org.bitbucket.linkingtweaks.tweaks.forcespawn;

import com.xcompwiz.mystcraft.api.linking.LinkEvent.LinkEventAlter;
import net.minecraftforge.event.ForgeSubscribe;
import org.bitbucket.linkingtweaks.Log;
import org.bitbucket.linkingtweaks.api.lib.LinkFlags;
import org.bitbucket.linkingtweaks.tweaks.TweakListener;

public class ListenerForceSpawn implements TweakListener {
    private String[] bypassFlags = {LinkFlags.NATURAL.flag, LinkFlags.TPX.flag};

    @ForgeSubscribe
    public void checkGroups(LinkEventAlter event) {
        if(TweakForceSpawn.info.isEnabled()) {
            if(LinkFlags.doesNotContainFlags(event.options, bypassFlags)) {
                logRedirectedLink(event);
                event.spawn = event.destination.getSpawnPoint();
            }
        }
    }

    private void logRedirectedLink(LinkEventAlter event) {
        String worldName = event.destination.getWorldInfo().getWorldName();
        Log.logInfo("Entity %s linked to %s, redirecting link destination to %s spawn", event.entity, worldName, worldName);
    }
}
