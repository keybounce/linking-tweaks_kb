package org.bitbucket.linkingtweaks.tweaks.peacefuldimensions;

import net.minecraft.entity.monster.EntityMob;
import net.minecraftforge.event.Event;
import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.entity.living.LivingSpawnEvent;
import org.bitbucket.linkingtweaks.tweaks.TweakListener;

/** @author Veovis Muad'dib */
public class ListenerPeacefulDimensions implements TweakListener {
    @ForgeSubscribe
    public void spawnEvent(LivingSpawnEvent.CheckSpawn event) {
        if(TweakPeacefulDimensions.info.isEnabled()) {
            if(event.entityLiving instanceof EntityMob) {
                if(TweakPeacefulDimensions.info.isDimensionPeaceful(event.entityLiving.dimension)) {
                    event.setResult(Event.Result.DENY);
                }
            }
        }
    }
}
