package org.bitbucket.linkingtweaks.tweaks.peacefuldimensions;

import org.bitbucket.linkingtweaks.LinkingTweaks;
import org.bitbucket.linkingtweaks.tweaks.TweakInfo;

/** @author Veovis Muad'dib */
public class InfoPeacefulDimensions extends TweakInfo {
    public int[] dimensions;

    InfoPeacefulDimensions() {
        CONFIG_NAME = "peacefuldimensions";
        CONFIG_COMMENT = "Set specific dimensions to not spawn hostile mobs.";
        enabled = LinkingTweaks.CFG.addTweakConfig(CONFIG_NAME, CONFIG_COMMENT, false);

        dimensions = LinkingTweaks.CFG.addIntArrayConfig(CONFIG_NAME + ".ids", null, -2);
    }

    public boolean isDimensionPeaceful(int dimension) {
        boolean value = false;
        for(int dim : dimensions) {
            if(dim == dimension) {
                value = true;
                break;
            }
        }
        return value;
    }
}
