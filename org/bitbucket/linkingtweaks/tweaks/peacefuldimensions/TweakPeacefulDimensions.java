package org.bitbucket.linkingtweaks.tweaks.peacefuldimensions;

import org.bitbucket.linkingtweaks.tweaks.Tweak;
import org.bitbucket.linkingtweaks.tweaks.TweakInfo;

/** @author Veovis Muad'dib */
public class TweakPeacefulDimensions extends Tweak {
    public static InfoPeacefulDimensions info;

    public TweakPeacefulDimensions() {
        super(new ListenerPeacefulDimensions());
    }

    @Override
    public TweakInfo getInfo() {
        return info;
    }

    @Override
    public void configure() {
        info = new InfoPeacefulDimensions();
    }
}
