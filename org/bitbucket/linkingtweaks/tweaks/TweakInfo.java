package org.bitbucket.linkingtweaks.tweaks;

/** @author Veovis Muad'dib */
public class TweakInfo {
    protected String CONFIG_NAME;
    protected String CONFIG_COMMENT;

    protected boolean enabled = false;

    public String getName() {
        return CONFIG_NAME;
    }

    public String getComment() {
        return CONFIG_COMMENT;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean value) {
        enabled = value;
    }
}
