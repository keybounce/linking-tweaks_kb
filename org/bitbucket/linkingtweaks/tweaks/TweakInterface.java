package org.bitbucket.linkingtweaks.tweaks;

import org.bitbucket.linkingtweaks.LinkingTweaks;
import org.bitbucket.linkingtweaks.api.tweaks.TweakAPI;

import java.util.ArrayList;
import java.util.List;

/** @author Veovis Muad'dib */
public class TweakInterface implements TweakAPI {
    public static final List<Tweak> tweaks = LinkingTweaks.tweaks;

    @Override
    public List<String> getTweakNames() {
        List<String> tweakNames = new ArrayList<String>();
        for(Tweak tweak : tweaks) {
            tweakNames.add(tweak.getConfigName());
        }
        return tweakNames;
    }

    @Override
    public boolean tweakExists(String name) {
        boolean value = false;
        List<String> names = getTweakNames();
        if(names.contains(name)) {
            value = true;
        }
        return value;
    }

    @Override
    public boolean isEnabled(String name) {
        boolean value = false;
        if(tweakExists(name)) {
            Tweak tweak = getTweakFromName(name);
            value = tweak.getInfo().isEnabled();
        }
        return value;
    }

    @Override
    public boolean setEnabled(String name, boolean value) {
        if(tweakExists(name)) {
            Tweak tweak = getTweakFromName(name);
            tweak.getInfo().setEnabled(value);
        }
        return value;
    }

    @Override
    public boolean toggleEnabled(String name) {
        boolean value = isEnabled(name);
        value = setEnabled(name, !value);
        return value;
    }

    private Tweak getTweakFromName(String name) {
        Tweak value = null;
        for(Tweak tweak : tweaks) {
            if(name.toLowerCase().equals(tweak.getConfigName())) {
                value = tweak;
                break;
            }
        }
        return value;
    }
}
