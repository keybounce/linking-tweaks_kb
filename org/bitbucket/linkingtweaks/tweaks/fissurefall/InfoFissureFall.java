package org.bitbucket.linkingtweaks.tweaks.fissurefall;

import org.bitbucket.linkingtweaks.LinkingTweaks;
import org.bitbucket.linkingtweaks.tweaks.TweakInfo;

public class InfoFissureFall extends TweakInfo {
    public final boolean RESCUE;
    public final int RESCUE_TIMEFRAME;
    public final int RESCUE_HEALTH_THRESHOLD;
    public final boolean RESCUE_NO_DAMAGE;

    InfoFissureFall() {
        CONFIG_NAME = "fissurefall";
        CONFIG_COMMENT = "Entities falling through a Star Fissure will fall towards Overworld spawn rather than link there.";
        enabled = LinkingTweaks.CFG.addTweakConfig(CONFIG_NAME, CONFIG_COMMENT, true);

        RESCUE = LinkingTweaks.CFG.addBooleanConfig("fissurefall.rescue", null, true);
        RESCUE_NO_DAMAGE = LinkingTweaks.CFG.addBooleanConfig("fissurefall.rescue.nodamage", null, false);
        RESCUE_HEALTH_THRESHOLD = LinkingTweaks.CFG.addIntConfig("fissurefall.rescue.healththreshold", null, 3);
        RESCUE_TIMEFRAME = LinkingTweaks.CFG.addIntConfig("fissurefall.rescue.ticktimeframe", null, 200);
    }
}
