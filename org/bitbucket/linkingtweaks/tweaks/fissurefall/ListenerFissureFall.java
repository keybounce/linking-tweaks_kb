package org.bitbucket.linkingtweaks.tweaks.fissurefall;

import com.xcompwiz.mystcraft.api.linking.LinkEvent.LinkEventAlter;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.DamageSource;
import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import org.bitbucket.linkingtweaks.Log;
import org.bitbucket.linkingtweaks.api.lib.LinkFlags;
import org.bitbucket.linkingtweaks.tweaks.TweakListener;

import java.util.Map;
import java.util.WeakHashMap;

public class ListenerFissureFall implements TweakListener {
    private Map<EntityLivingBase, Long> entitiesToRescue = new WeakHashMap<EntityLivingBase, Long>();

    @ForgeSubscribe
    public void redirectFissures(LinkEventAlter event) {
        if(TweakFissureFall.info.isEnabled()) {
            if(LinkFlags.linkContainsFlags(event.options, LinkFlags.FISSURE.flag)) {
                logRedirectedLink(event);
                event.spawn = event.options.getSpawn();
                event.spawn.posY = event.destination.getActualHeight() + 100;
                if(event.entity instanceof EntityLivingBase) {
                    entitiesToRescue.put((EntityLivingBase) event.entity, event.entity.worldObj.getTotalWorldTime());
                }
            }
        }
    }

    @ForgeSubscribe
    public void reduceFallDamage(LivingHurtEvent event) {
        if(TweakFissureFall.info.isEnabled()) {
            if(entitiesToRescue.containsKey(event.entityLiving)) {
                long currentTime = event.entityLiving.worldObj.getTotalWorldTime();
                if(currentTime - entitiesToRescue.get(event.entityLiving) < TweakFissureFall.info.RESCUE_TIMEFRAME) {
                    if(event.source == DamageSource.fall && TweakFissureFall.info.RESCUE) {
                        rescueEntity(event);
                    }
                } else {
                    entitiesToRescue.remove(event.entityLiving);
                }
            }
        }
    }

    private void rescueEntity(LivingHurtEvent event) {
        logRescuedEntity(event);
        if(TweakFissureFall.info.RESCUE_NO_DAMAGE) {
            event.ammount = 0;
        } else {
            event.ammount = event.entityLiving.getHealth() - Math.max(0, TweakFissureFall.info.RESCUE_HEALTH_THRESHOLD);
        }
        entitiesToRescue.remove(event.entityLiving);
    }

    private void logRedirectedLink(LinkEventAlter event) {
        Log.logFine("%s traveled through a Star Fissure, redirecting destination to the sky.", event.entity.getEntityName());
    }

    private void logRescuedEntity(LivingHurtEvent event) {
        Log.logFine("Saving %s from lethal fall damage incurred from falling through a Star Fissure.", event.entity.getEntityName());
    }
}
