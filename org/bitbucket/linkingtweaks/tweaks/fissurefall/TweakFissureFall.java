package org.bitbucket.linkingtweaks.tweaks.fissurefall;

import org.bitbucket.linkingtweaks.tweaks.Tweak;
import org.bitbucket.linkingtweaks.tweaks.TweakInfo;

/**
 * Redirects Star Fissure links to high up in the sky.
 * Also prevents redirected entities from taking lethal fall damage due to the redirection.
 *
 * @author Veovis Muad'dib
 */
public class TweakFissureFall extends Tweak {
    static InfoFissureFall info;

    public TweakFissureFall() {
        super(new ListenerFissureFall());
    }

    @Override
    public TweakInfo getInfo() {
        return info;
    }

    @Override
    public void configure() {
        info = new InfoFissureFall();
    }
}
