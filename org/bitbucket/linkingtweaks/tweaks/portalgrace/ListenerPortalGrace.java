package org.bitbucket.linkingtweaks.tweaks.portalgrace;

import com.xcompwiz.mystcraft.api.linking.LinkEvent;
import com.xcompwiz.mystcraft.api.linking.LinkEvent.LinkEventAllow;
import com.xcompwiz.mystcraft.api.linking.LinkEvent.LinkEventStart;
import net.minecraft.entity.Entity;
import net.minecraftforge.event.ForgeSubscribe;
import org.bitbucket.linkingtweaks.api.lib.LinkFlags;
import org.bitbucket.linkingtweaks.tweaks.TweakListener;

import java.util.Map;
import java.util.WeakHashMap;

/** @author Veovis Muad'dib */
public class ListenerPortalGrace implements TweakListener {
    private Map<Entity, Long> entities = new WeakHashMap<Entity, Long>();

    @ForgeSubscribe
    public void enforceGracePeriod(LinkEventAllow event) {
        if(shouldProcess(event)) {
            process(event);
        }
    }

    @ForgeSubscribe
    public void resetGracePeriod(LinkEventStart event) {
        if(TweakPortalGrace.info.isEnabled()) {
            entities.put(event.entity, event.entity.worldObj.getTotalWorldTime());
        }
    }

    private boolean shouldProcess(LinkEvent event) {
        boolean value = false;
        if(TweakPortalGrace.info.isEnabled()) {
            if(LinkFlags.linkContainsFlags(event.options, LinkFlags.PORTAL.flag)) {
                if(entities.containsKey(event.entity)) {
                    value = true;
                }
            }
        }
        return value;
    }

    private void process(LinkEvent event) {
        long currentTime = event.entity.worldObj.getTotalWorldTime();
        if(currentTime - entities.get(event.entity) < TweakPortalGrace.info.GRACE_PERIOD) {
            event.setCanceled(true);
        } else {
            entities.remove(event.entity);
        }
    }
}
