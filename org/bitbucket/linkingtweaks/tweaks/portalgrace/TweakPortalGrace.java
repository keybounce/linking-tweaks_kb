package org.bitbucket.linkingtweaks.tweaks.portalgrace;

import org.bitbucket.linkingtweaks.tweaks.Tweak;
import org.bitbucket.linkingtweaks.tweaks.TweakInfo;

/** @author Veovis Muad'dib */
public class TweakPortalGrace extends Tweak {
    static InfoPortalGrace info;

    public TweakPortalGrace() {
        super(new ListenerPortalGrace());
    }

    @Override
    public TweakInfo getInfo() {
        return info;
    }

    @Override
    public void configure() {
        info = new InfoPortalGrace();
    }
}
