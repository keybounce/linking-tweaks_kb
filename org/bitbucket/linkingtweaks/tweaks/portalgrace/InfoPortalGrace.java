package org.bitbucket.linkingtweaks.tweaks.portalgrace;

import org.bitbucket.linkingtweaks.LinkingTweaks;
import org.bitbucket.linkingtweaks.tweaks.TweakInfo;

/** @author Veovis Muad'dib */
public class InfoPortalGrace extends TweakInfo {
    public int GRACE_PERIOD;

    InfoPortalGrace() {
        CONFIG_NAME = "portalgrace";
        CONFIG_COMMENT = "Provides a small grace period after linking, during which no portal links will be activated.";
        enabled = LinkingTweaks.CFG.addTweakConfig(CONFIG_NAME, CONFIG_COMMENT, false);

        GRACE_PERIOD = Math.max(0, LinkingTweaks.CFG.addIntConfig(CONFIG_NAME + ".ticks", null, 20));
    }
}
