package org.bitbucket.linkingtweaks.tweaks.agegroups;

import com.xcompwiz.mystcraft.api.linking.LinkEvent.LinkEventAllow;
import net.minecraftforge.event.ForgeSubscribe;
import org.bitbucket.linkingtweaks.Log;
import org.bitbucket.linkingtweaks.api.LinkingTweaksAPI;
import org.bitbucket.linkingtweaks.api.MystLib;
import org.bitbucket.linkingtweaks.api.lib.LinkFlags;
import org.bitbucket.linkingtweaks.tweaks.TweakListener;

public class ListenerAgeGroups implements TweakListener {
    private String[] bypassFlags = {LinkFlags.INTRA.flag, LinkFlags.NATURAL.flag, LinkFlags.TPX.flag};

    @ForgeSubscribe
    public void checkGroups(LinkEventAllow event) {
        if(TweakAgeGroups.info.isEnabled()) {
            if(LinkFlags.doesNotContainFlags(event.options, bypassFlags)) {
                processLink(event);
            }
        }
    }

    private void processLink(LinkEventAllow event) {
        logProcessingLink(event);
        int origin = MystLib.ages.getDimIdFromEntity(event.entity);
        int destination = event.options.getDimensionUID();
        if(LinkingTweaksAPI.ageGroups.dimensionsInAnyGrouping(origin, destination)) {
            logCancelledLink(event);
            event.setCanceled(true);
        }
    }

    private void logProcessingLink(LinkEventAllow event) {
        Log.logFiner("%s attempting to link from %s to %s.  Checking against age groups!", event.entity.getEntityName(), MystLib.ages.getDimIdFromEntity(event.entity), event.options.getDimensionUID());
    }

    private void logCancelledLink(LinkEventAllow event) {
        Log.logSevere("Prevented %s from linking from %s to %s, as they are in the same age group", event.entity.getEntityName(), MystLib.ages.getDimIdFromEntity(event.entity), event.options.getDimensionUID());
    }
}
