package org.bitbucket.linkingtweaks.tweaks.agegroups;

import org.bitbucket.linkingtweaks.api.tweaks.AgeGroupsAPI;

public class AgeGroupsInterface implements AgeGroupsAPI {
    @Override
    public void registerNewAgeGroup(int... dimensionIds) {
        TweakAgeGroups.info.addGroup(new AgeGroup(dimensionIds));
    }

    @Override
    public boolean dimensionsInAnyGrouping(int... dimensionIds) {
        boolean value = false;
        if(TweakAgeGroups.info.isEnabled()) {
            value = TweakAgeGroups.info.inAnyGrouping(dimensionIds);
        }
        return value;
    }
}
