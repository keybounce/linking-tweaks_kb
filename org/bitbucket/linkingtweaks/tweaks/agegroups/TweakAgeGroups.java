package org.bitbucket.linkingtweaks.tweaks.agegroups;

import org.bitbucket.linkingtweaks.api.LinkingTweaksAPI;
import org.bitbucket.linkingtweaks.tweaks.Tweak;
import org.bitbucket.linkingtweaks.tweaks.TweakInfo;

/**
 * Prevents non-intra-linking between grouped dimensions.
 *
 * @author Veovis Muad'dib
 * @see {@link AgeGroup}
 */
public class TweakAgeGroups extends Tweak {
    static InfoAgeGroups info;

    public TweakAgeGroups() {
        super(new ListenerAgeGroups());
    }

    @Override
    public TweakInfo getInfo() {
        return info;
    }

    @Override
    public void configure() {
        info = new InfoAgeGroups();
    }

    @Override
    public void registerAPI() {
        AgeGroupsInterface api = new AgeGroupsInterface();
        LinkingTweaksAPI.ageGroups = api;
    }
}