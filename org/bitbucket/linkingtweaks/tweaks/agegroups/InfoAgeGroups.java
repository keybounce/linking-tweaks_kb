package org.bitbucket.linkingtweaks.tweaks.agegroups;

import org.bitbucket.linkingtweaks.LinkingTweaks;
import org.bitbucket.linkingtweaks.tweaks.TweakInfo;

import java.util.ArrayList;
import java.util.List;

/** @author Veovis Muad'dib */
public class InfoAgeGroups extends TweakInfo {
    private List<AgeGroup> groups = new ArrayList<AgeGroup>();

    InfoAgeGroups() {
        CONFIG_NAME = "agegroups";
        CONFIG_COMMENT = "Allow grouping multiple dimensions together to be treated as a single age.";
        enabled = LinkingTweaks.CFG.addTweakConfig(CONFIG_NAME, CONFIG_COMMENT, false);

        int numberOfGroups = LinkingTweaks.CFG.addIntConfig(CONFIG_NAME + ".numberofgroups", null, 1);
        for(int i = 0; i < numberOfGroups; i++) {
            int[] group = LinkingTweaks.CFG.addIntArrayConfig(CONFIG_NAME + ".group." + i, null, -1, 0, 1);
            groups.add(new AgeGroup(group));
        }
    }

    public void addGroup(AgeGroup group) {
        groups.add(group);
    }

    public boolean inAnyGrouping(int... dims) {
        boolean value = false;
        for(AgeGroup group : groups) {
            if(group.inGroup(dims) || value) {
                value = true;
            }
        }
        return value;
    }
}
