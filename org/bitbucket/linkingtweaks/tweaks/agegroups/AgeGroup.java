package org.bitbucket.linkingtweaks.tweaks.agegroups;

import java.util.ArrayList;
import java.util.List;

/**
 * Defines a group of dimensions to be treated as a single Mystcraft age.
 * //TODO: Possibly not needed anymore, may move to int arrays again and track them in Info.
 *
 * @author Veovis Muad'dib
 */
public class AgeGroup {
    private List<Integer> dims = new ArrayList<Integer>();

    /**
     * Creates a new Group with the specified dimensions.
     *
     * @param dimensions The dimension ids that will make up the Group.
     */
    public AgeGroup(int... dimensions) {
        if(dimensions.length > 0) {
            for(int dim : dimensions) {
                dims.add(dim);
            }
        }
    }

    /**
     * Checks for the given dimension's presence within this Group.
     *
     * @param dim The dimension id to check for.
     * @return True if the given dimension id is contained within this Group.
     */
    public boolean contains(int dim) {
        return dims.contains(dim);
    }

    /**
     * Checks for the presence of both given dimensions within this Group.
     *
     * @param dims The dimension ids to check for.
     * @return True if all dimension ids are contained within this Group.
     */
    public boolean inGroup(int... dims) {
        boolean value = true;
        for(int i : dims) {
            if(!this.contains(i) || !value) {
                value = false;
            }
        }
        return value;
    }

}