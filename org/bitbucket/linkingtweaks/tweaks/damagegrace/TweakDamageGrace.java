package org.bitbucket.linkingtweaks.tweaks.damagegrace;

import org.bitbucket.linkingtweaks.tweaks.Tweak;
import org.bitbucket.linkingtweaks.tweaks.TweakInfo;

/** @author Veovis Muad'dib */
public class TweakDamageGrace extends Tweak {
    static InfoDamageGrace info;

    public TweakDamageGrace() {
        super(new ListenerDamageGrace());
    }

    @Override
    public TweakInfo getInfo() {
        return info;
    }

    @Override
    public void configure() {
        info = new InfoDamageGrace();
    }
}
