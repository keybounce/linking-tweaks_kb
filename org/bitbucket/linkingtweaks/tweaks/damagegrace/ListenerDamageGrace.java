package org.bitbucket.linkingtweaks.tweaks.damagegrace;

import com.xcompwiz.mystcraft.api.linking.LinkEvent.LinkEventEnd;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import org.bitbucket.linkingtweaks.tweaks.TweakListener;

import java.util.Map;
import java.util.WeakHashMap;

/** @author Veovis Muad'dib */
public class ListenerDamageGrace implements TweakListener {
    private Map<EntityLivingBase, Long> entities = new WeakHashMap<EntityLivingBase, Long>();

    @ForgeSubscribe
    public void enforceGracePeriod(LivingHurtEvent event) {
        if(TweakDamageGrace.info.isEnabled()) {
            if(entities.containsKey(event.entityLiving)) {
                if(getTime(event.entity) - entities.get(event.entityLiving) < TweakDamageGrace.info.GRACE_PERIOD) {
                    event.setCanceled(true);
                } else {
                    entities.remove(event.entityLiving);
                }
            }
        }
    }

    @ForgeSubscribe
    public void resetGracePeriod(LinkEventEnd event) {
        if(TweakDamageGrace.info.isEnabled()) {
            if(event.entity instanceof EntityLivingBase) {
                EntityLivingBase entityLiving = (EntityLivingBase) event.entity;
                entities.put(entityLiving, getTime(entityLiving));
            }
        }
    }

    private long getTime(Entity entity) {
        return entity.worldObj.getTotalWorldTime();
    }
}
