package org.bitbucket.linkingtweaks.tweaks.damagegrace;

import org.bitbucket.linkingtweaks.LinkingTweaks;
import org.bitbucket.linkingtweaks.tweaks.TweakInfo;

/** @author Veovis Muad'dib */
public class InfoDamageGrace extends TweakInfo {
    public int GRACE_PERIOD;

    InfoDamageGrace() {
        CONFIG_NAME = "damagegrace";
        CONFIG_COMMENT = "Provides an invulnerability grace period after linking.";
        enabled = LinkingTweaks.CFG.addTweakConfig(CONFIG_NAME, CONFIG_COMMENT, false);

        GRACE_PERIOD = LinkingTweaks.CFG.addIntConfig(CONFIG_NAME + ".ticks", null, 40);
    }
}
