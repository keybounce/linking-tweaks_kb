package org.bitbucket.linkingtweaks.api.lib;

import com.xcompwiz.mystcraft.api.linking.ILinkInfo;
import com.xcompwiz.mystcraft.api.linking.ILinkPropertyAPI;

/**
 * Provides a single place to look for all linking flags that Linking Tweaks is aware of.
 * Also provides basic helper functions for working with linking flags.
 *
 * @author Veovis Muad'dib
 */
public enum LinkFlags {
    DISARM(ILinkPropertyAPI.FLAG_DISARM),
    EXTERNAL(ILinkPropertyAPI.FLAG_EXTERNAL),
    FISSURE("starfissure"),
    FOLLOW(ILinkPropertyAPI.FLAG_FOLLOWING),
    INTRA(ILinkPropertyAPI.FLAG_INTRA_LINKING),
    MAINTAIN_MOMENTUM(ILinkPropertyAPI.FLAG_MAINTAIN_MOMENTUM),
    NATURAL(ILinkPropertyAPI.FLAG_NATURAL),
    OFFENSIVE(ILinkPropertyAPI.FLAG_OFFENSIVE),
    PLATFORM(ILinkPropertyAPI.FLAG_GENERATE_PLATFORM),
    PORTAL("portal"),
    RELATIVE(ILinkPropertyAPI.FLAG_RELATIVE),
    TPX(ILinkPropertyAPI.FLAG_TPCOMMAND);

    public final String flag;

    LinkFlags(String flag) {
        this.flag = flag;
    }

    public static boolean linkContainsFlags(ILinkInfo info, String... flags) {
        boolean value = false;
        for(String flag : flags) {
            if(info.getFlag(flag)) {
                value = true;
            }
        }
        return value;
    }

    public static boolean doesNotContainFlags(ILinkInfo info, String... flags) {
        return !linkContainsFlags(info, flags);
    }
}