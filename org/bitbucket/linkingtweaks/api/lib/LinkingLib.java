package org.bitbucket.linkingtweaks.api.lib;

import com.xcompwiz.mystcraft.api.MystAPI;
import com.xcompwiz.mystcraft.api.linking.ILinkInfo;
import com.xcompwiz.mystcraft.api.linking.ILinkPropertyAPI;

/** @author Veovis Muad'dib */
public class LinkingLib {
    public ILinkInfo createFissureLink(int dimensionId) {
        ILinkInfo link = MystAPI.linking.createLinkInfo(null);
        link.setDimensionUID(dimensionId);
        link.setFlag(LinkFlags.EXTERNAL.flag, true);
        link.setFlag(LinkFlags.INTRA.flag, true);
        link.setFlag(LinkFlags.NATURAL.flag, true);
        link.setFlag(LinkFlags.FISSURE.flag, true);
        link.setProperty(ILinkPropertyAPI.PROP_SOUND, "mystcraft:linking.link-fissure");
        return link;
    }

    public ILinkInfo createFissureLink() {
        return createFissureLink(0);
    }
}
