package org.bitbucket.linkingtweaks.api.lib;

import net.minecraft.entity.Entity;
import net.minecraft.world.World;
import org.bitbucket.linkingtweaks.api.LinkingTweaksAPI;

public class AgeLib {
    public AgeLib() {
    }

    /**
     * Helper function to check whether a pair of dimension ids represent the same "Age".
     * Checks ids for the same value as well as checking enabled general that alter Intra-linking mechanics, such as AgeGroups.
     */
    public boolean isIntraLink(int origin, int destination) {
        boolean value = false;
        if(origin == destination) {
            value = true;
        } else if(LinkingTweaksAPI.ageGroups != null) {
            value = LinkingTweaksAPI.ageGroups.dimensionsInAnyGrouping(origin, destination);
        }
        return value;
    }

    /**
     * Just a shortcut to find the dim id of a world object.  Minimizes impact of Minecraft updates.
     *
     * @throws IllegalArgumentException on null argument.
     */
    public int getDimIdFromWorld(World world) {
        if(world == null) {
            throw new IllegalArgumentException("World cannot be null!");
        }
        return world.provider.dimensionId;
    }

    /**
     * Just a shortcut to find the dim id of an entity.  Minimizes impact of Minecraft updates.
     *
     * @throws IllegalArgumentException on null argument.
     */
    public int getDimIdFromEntity(Entity entity) {
        if(entity == null) {
            throw new IllegalArgumentException("Entity cannot be null!");
        }
        return entity.dimension;
    }
}
