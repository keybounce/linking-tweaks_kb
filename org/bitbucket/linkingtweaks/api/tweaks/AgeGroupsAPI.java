package org.bitbucket.linkingtweaks.api.tweaks;

public interface AgeGroupsAPI {
    /**
     * Registers a new group of dimensions to be treated as a single age.
     * @param dimensionIds
     */
    public void registerNewAgeGroup(int... dimensionIds);

    /**
     * Returns true if all given dimensions are contained inside a single Age Group.
     */
    public boolean dimensionsInAnyGrouping(int... dimensionIds);
}
