package org.bitbucket.linkingtweaks.api.tweaks;

import java.util.List;

/** @author Veovis Muad'dib */
public interface TweakAPI {
    public List<String> getTweakNames();

    public boolean tweakExists(String name);

    public boolean isEnabled(String name);

    public boolean setEnabled(String name, boolean value);

    public boolean toggleEnabled(String name);
}
