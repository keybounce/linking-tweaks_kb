package org.bitbucket.linkingtweaks.api;

import org.bitbucket.linkingtweaks.api.tweaks.AgeGroupsAPI;
import org.bitbucket.linkingtweaks.api.tweaks.TweakAPI;

/**
 * Provides interactions with various general, if they are enabled.
 * Also provides various helpers for interacting with Mystcraft.
 * These are not guaranteed to be set.  Be sure to check for nulls.
 * These are set during Linking Tweaks' pre-init phase
 *
 * @author Veovis Muad'dib
 */
public class LinkingTweaksAPI {
    //APIs for interacting with Tweaks specifically.
    public static TweakAPI general = null;
    public static AgeGroupsAPI ageGroups = null;
}
