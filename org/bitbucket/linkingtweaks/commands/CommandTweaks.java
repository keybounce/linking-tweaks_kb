package org.bitbucket.linkingtweaks.commands;

import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import org.bitbucket.linkingtweaks.Log;
import org.bitbucket.linkingtweaks.api.LinkingTweaksAPI;

import java.util.ArrayList;
import java.util.List;

/** @author Veovis Muad'dib */
public class CommandTweaks extends CommandCustom {
    public CommandTweaks() {
        COMMAND_NAME = "linktweaks";
        USAGE = "/" + COMMAND_NAME + " <tweakname> [true|false]";
    }

    @Override
    public void processCommand(ICommandSender sender, String[] args) {
        if(args.length == 1) {
            toggleEnabledStatus(sender, args);
        } else if(args.length == 2) {
            setEnabledStatus(sender, args);
        } else {
            throw new WrongUsageException(USAGE);
        }
    }

    private void toggleEnabledStatus(ICommandSender sender, String[] args) {
        String tweakName = args[0];
        if(LinkingTweaksAPI.general.tweakExists(tweakName)) {
            LinkingTweaksAPI.general.toggleEnabled(tweakName);
            logChangedStatus(sender, tweakName);
        } else {
            throw new WrongUsageException(USAGE);
        }
    }

    private void setEnabledStatus(ICommandSender sender, String[] args) {
        String tweakName = args[0];
        boolean enable;
        if(LinkingTweaksAPI.general.tweakExists(tweakName)) {
            try {
                enable = Boolean.parseBoolean(args[1]);
            } catch(Exception e) {
                throw new WrongUsageException(USAGE);
            }
            logChangedStatus(sender, tweakName);
            LinkingTweaksAPI.general.setEnabled(tweakName, enable);
        } else {
            throw new WrongUsageException(USAGE);
        }
    }

    @Override
    public List addTabCompletionOptions(ICommandSender par1ICommandSender, String[] args) {
        List options = new ArrayList();
        if(args.length == 1) {
            options = getTweakAutoCompletions(args[0]);
        } else if(args.length == 2) {
            options = getBoolAutoCompletions(args[1]);
        }
        return options;
    }

    private List getTweakAutoCompletions(String arg) {
        List options = new ArrayList();
        List<String> names = LinkingTweaksAPI.general.getTweakNames();
        for(String name : names) {
            if(doesStringStartWith(arg, name.toLowerCase())) {
                options.add(name);
            }
        }
        return options;
    }

    private void logChangedStatus(ICommandSender sender, String tweakName) {
        String notification = String.format("%s toggled %s state to %s", sender.getCommandSenderName(), tweakName, LinkingTweaksAPI.general.isEnabled(tweakName));
        Log.logInfo(notification);
        notifyAdmins(sender, notification);
    }
}
