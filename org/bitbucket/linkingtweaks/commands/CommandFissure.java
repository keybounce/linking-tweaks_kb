package org.bitbucket.linkingtweaks.commands;

import com.xcompwiz.mystcraft.api.MystAPI;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.entity.player.EntityPlayerMP;
import org.bitbucket.linkingtweaks.Log;
import org.bitbucket.linkingtweaks.api.MystLib;

import java.util.ArrayList;
import java.util.List;

/** @author Veovis Muad'dib */
public class CommandFissure extends CommandCustom {
    public CommandFissure() {
        COMMAND_NAME = "fissure";
        USAGE = "/" + COMMAND_NAME + " [player] [dimension]";
    }

    @Override
    public void processCommand(ICommandSender sender, String[] args) {
        EntityPlayerMP subject = null;
        int target = 0;
        if(args.length == 0) {
            subject = getCommandSenderAsPlayer(sender);
        } else if(args.length == 1) {
            EntityPlayerMP player = tryGetPlayer(sender, args[0]);
            if(player == null) {
                subject = getCommandSenderAsPlayer(sender);
                target = getInt(args[0]);
            } else {
                subject = player;
            }
        } else if(args.length == 2) {
            subject = tryGetPlayer(sender, args[0]);
            target = getInt(args[1]);
        }

        if(subject != null) {
            Log.logFine("%s sent %s through a Star Fissure to %s!", sender.getCommandSenderName(), subject.getDisplayName(), target);
            MystAPI.linking.linkEntity(subject, MystLib.linking.createFissureLink(target));
        } else {
            throw new WrongUsageException(USAGE);
        }
    }

    @Override
    public List addTabCompletionOptions(ICommandSender sender, String[] args) {
        List options = new ArrayList();
        if(args.length == 1) {
            options = getPlayerAutocompletions(args[0]);
        } else if(args.length == 2) {
            if(doesStringStartWith(args[2], "0")) {
                options.add("0");
            }
        }
        return options;
    }
}
