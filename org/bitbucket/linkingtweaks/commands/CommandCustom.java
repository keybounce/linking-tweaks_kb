package org.bitbucket.linkingtweaks.commands;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.PlayerNotFoundException;
import net.minecraft.command.WrongUsageException;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;

import java.util.ArrayList;
import java.util.List;

/** @author Veovis Muad'dib */
public abstract class CommandCustom extends CommandBase {
    protected String COMMAND_NAME;
    protected String USAGE;
    protected int PERMISSION_LEVEL = 2;

    @Override
    public String getCommandName() {
        return COMMAND_NAME;
    }

    @Override
    public String getCommandUsage(ICommandSender icommandsender) {
        return USAGE;
    }

    @Override
    public int getRequiredPermissionLevel() {
        return PERMISSION_LEVEL;
    }

    protected EntityPlayerMP tryGetPlayer(ICommandSender sender, String name) {
        EntityPlayerMP player = null;
        try {
            player = getPlayer(sender, name);
        } catch(PlayerNotFoundException e) {
            //Do nothing
        }
        return player;
    }

    protected int getInt(String string) {
        int value;
        try {
            value = Integer.parseInt(string);
        } catch(NumberFormatException e) {
            throw new WrongUsageException(USAGE);
        }
        return value;
    }

    protected List getBoolAutoCompletions(String arg) {
        String[] values = {"true", "false"};
        List options = new ArrayList();
        for(String value : values) {
            if(doesStringStartWith(arg, value.toLowerCase())) {
                options.add(value);
            }
        }
        return options;
    }

    protected List getPlayerAutocompletions(String text) {
        List options = new ArrayList();
        String[] playerNames = MinecraftServer.getServer().getAllUsernames();

        for(String playerName : playerNames) {
            if(doesStringStartWith(text, playerName)) {
                options.add(playerName);
            }
        }
        return options;
    }
}
