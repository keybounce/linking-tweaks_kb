package org.bitbucket.linkingtweaks;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.network.NetworkMod;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ServerCommandManager;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.common.MinecraftForge;
import org.bitbucket.linkingtweaks.api.LinkingTweaksAPI;
import org.bitbucket.linkingtweaks.api.MystLib;
import org.bitbucket.linkingtweaks.api.lib.AgeLib;
import org.bitbucket.linkingtweaks.api.lib.LinkingLib;
import org.bitbucket.linkingtweaks.commands.CommandFissure;
import org.bitbucket.linkingtweaks.commands.CommandTweaks;
import org.bitbucket.linkingtweaks.tweaks.Tweak;
import org.bitbucket.linkingtweaks.tweaks.TweakInterface;
import org.bitbucket.linkingtweaks.tweaks.agegroups.TweakAgeGroups;
import org.bitbucket.linkingtweaks.tweaks.damagegrace.TweakDamageGrace;
import org.bitbucket.linkingtweaks.tweaks.fissurefall.TweakFissureFall;
import org.bitbucket.linkingtweaks.tweaks.forcespawn.TweakForceSpawn;
import org.bitbucket.linkingtweaks.tweaks.linkcost.TweakLinkCost;
import org.bitbucket.linkingtweaks.tweaks.linksickness.TweakLinkSickness;
import org.bitbucket.linkingtweaks.tweaks.peacefuldimensions.TweakPeacefulDimensions;
import org.bitbucket.linkingtweaks.tweaks.portalgrace.TweakPortalGrace;
import org.bitbucket.linkingtweaks.tweaks.voidfissure.TweakVoidFissure;

import java.util.ArrayList;
import java.util.List;

/**
 * A server-side addon to Mystcraft, allowing admins to change the way linking works for their players.
 *
 * @author Veovis Muad'dib
 */
@Mod(
        modid = Version.modid,
        name = Version.modname,
        version = Version.version,
        dependencies = Version.depends) @NetworkMod(
        clientSideRequired = false,
        serverSideRequired = false)
public final class LinkingTweaks {
    public static final Config CFG = new Config("linking_tweaks.cfg");
    public static final List<Tweak> tweaks = new ArrayList<Tweak>();

    private static final List<CommandBase> commands = new ArrayList<CommandBase>();

    @Instance(Version.modid)
    public static LinkingTweaks instance = new LinkingTweaks();

    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        setupAPI();
        setupAdditionalFlags();
        addTweaks();
        configureTweaks();
        registerTweakAPIs();
        registerListeners();
    }

    @EventHandler
    public void serverStarting(FMLServerStartingEvent event) {
        addCommands();
        setupCommands();
    }

    private void setupAPI() {
        MystLib.ages = new AgeLib();
        MystLib.linking = new LinkingLib();
        LinkingTweaksAPI.general = new TweakInterface();
    }

    private void setupAdditionalFlags() {
        Log.logInfo("Adding flags for Star Fissures and Portals.");
        MinecraftForge.EVENT_BUS.register(new ListenerAddFlags());
    }

    private void addTweaks() {
        tweaks.add(new TweakAgeGroups());
        tweaks.add(new TweakDamageGrace());
        tweaks.add(new TweakFissureFall());
        tweaks.add(new TweakForceSpawn());
        tweaks.add(new TweakLinkCost());
        tweaks.add(new TweakLinkSickness());
        tweaks.add(new TweakPortalGrace());
        tweaks.add(new TweakPeacefulDimensions());
        tweaks.add(new TweakVoidFissure());
    }

    private void addCommands() {
        commands.add(new CommandFissure());
        commands.add(new CommandTweaks());
    }

    private void configureTweaks() {
        for(Tweak tweak : tweaks) {
            tweak.configure();
        }
    }

    private void registerTweakAPIs() {
        for(Tweak tweak : tweaks) {
            tweak.registerAPI();
        }
    }

    private void registerListeners() {
        for(Tweak tweak : tweaks) {
            tweak.registerListeners();
        }
    }

    private void setupCommands() {
        MinecraftServer server = MinecraftServer.getServer();
        ServerCommandManager manager = (ServerCommandManager) server.getCommandManager();
        for(CommandBase command : commands) {
            manager.registerCommand(command);
        }
    }
}