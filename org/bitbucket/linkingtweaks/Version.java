package org.bitbucket.linkingtweaks;

/**
 * Holds string literals for use in other parts of the mod.
 * @author Veovis Muad'dib
 */
public class Version {

    /**
     * The unique identifier for this mod.
     */
    public static final String modid   = "linkingtweaks";

    /**
     * The friendly name for this mod, to be displayed to the user.
     */
    public static final String modname = "Linking Tweaks";

    /**
     * The version number for this mod, set by the build script.
     */
    public static final String version = "@VERSION@";

    /**
     * The dependency field for this mod, ensures FML load order after the given mod identifier.
     * To be updated whenever updating the Mystcraft API.
     */
    public static final String depends = "required-after:Mystcraft@[0.10.6.06,)";
}
